-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2016 at 11:56 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `skproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `dates` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `dates`) VALUES
(16, 'Shihab Uddin ', '1987-04-04'),
(17, 'Ashraful Islam ', '2012-12-12'),
(18, 'Shahin uddin ', '1901-02-01'),
(20, 'sharif khan', '1994-03-10');

-- --------------------------------------------------------

--
-- Table structure for table `bookmark`
--

CREATE TABLE IF NOT EXISTS `bookmark` (
`id` int(11) NOT NULL,
  `title` varchar(60) NOT NULL,
  `url` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookmark`
--

INSERT INTO `bookmark` (`id`, `title`, `url`) VALUES
(1, 'w3schools', 'http://w3schools.com'),
(2, 'bdjobs', 'http://bdjobs.com'),
(3, 'php', 'http://php.net'),
(4, 'youtube', 'http://youtube.com'),
(5, 'facebook', 'http://facebook.com'),
(6, 'flatuicolors', 'https://flatuicolors.com/'),
(9, 'FortAwesome', 'http://fortawesome.github.io/Font-Awesome/'),
(10, '', ''),
(11, '', ''),
(12, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `carmodel`
--

CREATE TABLE IF NOT EXISTS `carmodel` (
`id` int(11) NOT NULL,
  `carname` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carmodel`
--

INSERT INTO `carmodel` (`id`, `carname`, `model`) VALUES
(6, 'asf daf ds', 'fdsa fdsa f');

-- --------------------------------------------------------

--
-- Table structure for table `library`
--

CREATE TABLE IF NOT EXISTS `library` (
`id` int(11) NOT NULL,
  `title` varchar(66) NOT NULL,
  `author` varchar(66) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `library`
--

INSERT INTO `library` (`id`, `title`, `author`) VALUES
(1, 'PHP Book', 'Yameen Hossain '),
(2, 'CSS Book', 'Rashid '),
(3, 'HTML', 'SHARIF KHAN'),
(6, 'OOP Programming ', 'Nabila '),
(7, 'ASP.net ', 'Mr. Zadid ');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE IF NOT EXISTS `subscriptions` (
`id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `name`, `email`) VALUES
(1, 'Rashid', 'rashidrana.r@gmail.com'),
(2, 'Imran', 'mdkhan.imran29@gmail.com'),
(3, 'Arster', 'arster7777@yahoo.com'),
(4, 'Rana', 'rashidran.rashid@gmail.com'),
(5, 'Saiful', 'mdsaiful359@yahoo.com'),
(6, 'Raju', 'raju@gmail.com'),
(11, 'sharif khan', 'sharifwc@yahoo.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookmark`
--
ALTER TABLE `bookmark`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carmodel`
--
ALTER TABLE `carmodel`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `library`
--
ALTER TABLE `library`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `bookmark`
--
ALTER TABLE `bookmark`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `carmodel`
--
ALTER TABLE `carmodel`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `library`
--
ALTER TABLE `library`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
