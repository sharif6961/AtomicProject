<!DOCTYPE html>
<html>
<head><meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Atomic Project (SHARIF)  </title>
	<link rel="stylesheet" href="resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="resource/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container bg">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<h1><b>WEB APP DEVELOPMENT PHP BATCH - 10</b></h1>
					<h2>SHARIFUL ISLAM </h2>
					<h3>ID: 106831</h3>
					<hr>
					<h3><u>ALL ATOMIC PROJECT LIST</u></h3>
					<?php $folders = ["Birthdays","Library","Carmodel","Subscription","Tools"];?>
					<ul class="mainNav">
						<?php
							foreach($folders as $key => $folder){
								echo"<li><a href='views/SEIP106831/{$folder}/'>{$folder}</a></li>";
							}
						?>
					</ul>
				</div>
			</div>
		</div>
		<footer id="footer">
			<div class="footerArea">
				<div class="footer">
					<p>Design & Developed by SHARIF</p>
				</div>
			</div>		
		</footer>
	</div>
</body>
</html>