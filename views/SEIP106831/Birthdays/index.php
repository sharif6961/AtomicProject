<?php session_start();
	//ini_set("display_errors","off");
	/* function __autoload($className){
		$file = str_replace("\\","/", $className);
		require_once("../../../".$file.".php");
	}
	*/
	include_once("../../../vendor/autoload.php");
	use App\Bitm\SEIP106831\Birthdays\Birthday;
	use App\Bitm\SEIP106831\Utility\Utility;
	$obj = new Birthday;
	$results = $obj->index();
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Atomic Project (SHARIF) </title>
	<link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container bg">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<a href="../../../" class="back">&larr; BACK </a>
					<hr>
					<ul class="nav">
						<?php 
							$files = ["index","create"];
							$getid= 0;
							foreach($files as $key => $file){
								$name = ucfirst($file);
								$output ="<li><a class='";
									if($key == $getid){$output .= "active";}
								$output .= "' href='{$file}.php'>{$name}</a></li>";
								echo $output;
							}
						?>
					</ul>
					<hr>
					<h1>Birthday Information </h1>
					<div class="message"><?php echo Utility::message(); ?></div>
					<div class="listArea">
						
							<div class="row">
								<div class="col-xs-4 col-sm-3 col-md-2">
									<form class="ajax" action="#" method="post">
										<select class="items" name="items">
											<option value="15">15</option>
											<option value="20">20</option>
											<option value="30">30</option>
											<option value="40">40</option>
										</select>
									</form>
								</div>
								<div class="col-xs-8 col-sm-9 col-md-10">
									<div class="row">
										<div class="col-xs-5 col-sm-4 col-md-3">
											</div>
									</div>
								</div>
							</div>
						</div>
						<table class="table table-bordered">
							<thead class="text-center">
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Birthday</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody class="text-center">
								<?php 
									$slNo = 1;
									foreach($results as $result){
								?>
								<tr>
									<td><?php echo $slNo; ?></td>
									<td>
				<a href="single.php?id=<?php echo $result->id; ?>"><?php echo $result->name; ?></a>
									</td>
									<td>
				<a href="single.php?id=<?php echo $result->id; ?>"><?php echo Utility::changeFormat($result->dates); ?></a>
									</td>
									<td>
					<a href="edit.php?id=<?php echo $result->id; ?>" class="list-btn" title="Edit this?">
						<img src="../../../resource/images/edit.png">
					</a>
					<a href="tarsh.php?id=<?php echo $result->id; ?>" class="list-btn" title="Move to Trash?">
						<img src="../../../resource/images/tarsh.png">
					</a>
					<a href="delete.php?id=<?php echo $result->id; ?>" class="list-btn" title="Delete this?">
						<img src="../../../resource/images/delete.png">
					</a>
					<a href="share.php?id=<?php echo $result->id; ?>" class="list-btn" title="Share Your Friend?">
						<img src="../../../resource/images/share.png">
					</a>
									</td>
								</tr>
								<?php 
									++$slNo;
									}
								?>

							</tbody>
						</table>
					</div>
				
						</div>
				</div>
			</div>
			
		</div>
		<footer id="footer">
			<div class="footerArea">
				<div class="footer">
					<p>Design & Developed by  SHARIF </p>
				</div>
			</div>		
		</footer>
	</div>
</body>
</html>