<?php session_start();
	/* function __autoload($className){
		$file = str_replace("\\","/", $className);
		require_once("../../../".$file.".php"); }
	*/
	include_once("../../../vendor/autoload.php");
	use App\Bitm\SEIP106831\Birthdays\Birthday;
	use App\Bitm\SEIP106831\Utility\Utility;
	if(isset($_POST["submit"]) && isset($_GET["id"])){
		$obj = new Birthday($_POST,$_GET);
		$obj->update();
	}else{
		Utility::redirect("index.php");
	}
?>