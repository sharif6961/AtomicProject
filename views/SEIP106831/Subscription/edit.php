<?php  session_start();
	/* function __autoload($className){
		$file = str_replace("\\","/", $className);
		require_once("../../../".$file.".php"); }
	*/
	include_once("../../../vendor/autoload.php");
	use App\Bitm\SEIP106831\Subscription\Subscribe;
	use App\Bitm\SEIP106831\Utility\Utility;
	if(isset($_GET["id"])){
		$obj = new Subscribe(NULL,$_GET);
		$result = $obj->show();
	}else{
		Utility::redirect("index.php");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Atomic Project (SHARIF) </title>
	<link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container bg">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<a href="../../../" class="back">&larr; Back</a>
					<hr>
					<ul class="nav">
						<?php 
							$files = ["index","create"];
							$getid= 3;
							foreach($files as $key => $file){
								$name = ucfirst($file);
								$output ="<li><a class='";
									if($key == $getid){$output .= "active";}
								$output .= "' href='{$file}.php'>{$name}</a></li>";
								echo $output;
							}
						?>
					</ul>
					<hr>
					<h1>Edit Your Name and Email</h1>
					<div class="message"><?php echo Utility::message(); ?></div>
					<div class="formArea">
					<form class="form-horizontal" method="post" action="update.php?id=<?= $result->id; ?>">
					  <div class="form-group">
						<label class="col-sm-2 control-label">Your Name :</label>
						<div class="col-sm-6">
						  <input type="text" name="uName" class="form-control" placeholder="Name" value="<?= $result->name; ?>">
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-2 control-label">Your Email :</label>
						<div class="col-sm-6">
						  <input type="email" name="email" class="form-control" placeholder="Email" value="<?= $result->email; ?>">
						</div>
					  </div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" name="submit" class="btn btn-success">SUBMIT</button>
							</div>
						  </div>
					</form>
					
					</div>
				</div>
			</div>
		</div>
		<footer id="footer">
			<div class="footerArea">
				<div class="footer">
					<p>Design & Developed by SHARIF</p>
				</div>
			</div>		
		</footer>
	</div>
</body>
</html>



